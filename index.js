function convert(){
    let cnt = parseInt(document.getElementById('count').value);
    let val = parseFloat(document.getElementById('select'). selectedOptions[0]. value);
    let res = document.getElementById('result');
    res.innerText = val*cnt;
    if(res.innerText === 'NaN')
        res.innerText = "0.00";
}
axios
    .get('https://nbu.uz/uz/exchange-rates/json/')
    .then((res)=>{
        const array  = res.data;
        // console.log(array);
        array.sort((a, b) => b.nbu_buy_price - a.nbu_buy_price);
        const select = document.getElementById('select');
        const tbody = document.getElementById('tbody');
        array.forEach(el => {
            // console.log(el.code.slice(0,2).toLowerCase());
            select.innerHTML += `
                <option value="${el.cb_price}">${el.code}</option>`
            tbody.innerHTML += `
                <tr>
                    <td>
                    <span>
                        <img
                            class="flag"
                            src="https://flagcdn.com/h24/${el.code.slice(0,2).toLowerCase()}.png"
                            alt="this is ${el.code} flag" /></span
                        >1 ${el.title}, ${el.code}
                    </td>
                    <td>${el.nbu_buy_price}</td>
                    <td>${el.nbu_cell_price}</td>
                    <td>${el.cb_price}</td>
                </tr>
            `
        });
        
    })

    